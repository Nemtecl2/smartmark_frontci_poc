[![pipeline status on master](https://img.shields.io/gitlab/pipeline/Nemtecl/smartmark_frontci_poc/master)](https://gitlab.com/Nemtecl/smartmark_frontci_poc/pipelines)
[![pipeline status on develop](https://img.shields.io/gitlab/pipeline/Nemtecl/smartmark_frontci_poc/develop?label=build%20on%20develop)](https://gitlab.com/Nemtecl/smartmark_frontci_poc/pipelines)


# SmartmarkFrontciPoc

Projet Angular version `7.1.4` généré via [Angular CLI](https://github.com/angular/angular-cli).
Il s'agit d'une Proof of Concept pour la CI de notre projet annuel.

## Clonez le projet

Lancez `git clone https://gitlab.com/Nemtecl/smartmark_frontci_poc` dans un terminal (à l'endroit où vous souhaitez l'installer sur votre pc).

## Installez les dépendances

Lancez `npm install` avec npm ou `yarn install` avec yarn.

## Lancer le mode developpement

Lancez `ng serve` puis naviguez vers `http://localhost:4200/`. L'application rechargera automatiquement à chaque chargement.

## Build

Lancez `ng build` pour build le projet. Le résultat se trouvera dans le dossier `dist/`. Utilisez l'option `--prod` pour un build en mode production.

## Running unit tests

Lancez `ng test` pour exécuter les tests unitaires front via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Lancez `ng e2e` pour exécuter les tests end-to-end via [Protractor](http://www.protractortest.org/).

## CI

Le fichier `gitlab-ci.yml` contient les différents jobs de la chaîne d'intégration et de deploiement continu.
A chaque push, les tests unitaires sont lancés.
A chaque push sur `master`, on déploie en mode production sur un serveur via SSH.

## Membres

Cyrielle, Paul, Deelan, Melvin et Clément